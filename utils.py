
import pygame
import engine

item = pygame.image.load('./sprites/sugar.png')
def makeSugar(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,23,23)
    entityAnimation = engine.Animation([item])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'good'
    return entity

obstacle = pygame.image.load('./sprites/killer_feather.png')

def makeFeather(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,50,26)
    entityAnimation = engine.Animation([obstacle])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'bad'
    return entity

fairy = pygame.image.load('./sprites/th-fairy.png')

def makeFairy(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,28,25)
    entityAnimation = engine.Animation([fairy])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'bad'
    return entity

finish = pygame.image.load('./sprites/temp_finish.png')

def makeFinish(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,180,180)
    entityAnimation = engine.Animation([finish])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'win'
    return entity

finish2 = pygame.image.load('./sprites/pizzahut.png')
def makeFinish2(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,243,256)
    entityAnimation = engine.Animation([finish2])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'win'
    return entity

def makeFake(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,180,180)
    entityAnimation = engine.Animation([finish])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'good'
    return entity

wal_idle = pygame.image.load('./sprites/waligie/w-stand.png')

wal_walking0 = pygame.image.load('./sprites/waligie/w-stand.png')
wal_walking1 = pygame.image.load('./sprites/waligie/w-walk1.png')
wal_walking2 = pygame.image.load('./sprites/waligie/w-walk2.png')

def makeWal(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,32,56)
    entityIdleAnimation = engine.Animation([wal_idle])
    entityWalkingAnimation = engine.Animation([wal_walking0, wal_walking1, wal_walking2])
    entity.animations.add('idle', entityIdleAnimation)
    entity.animations.add('walk', entityWalkingAnimation)
    entity.type = 'player'
    return entity

wal_fly0 = pygame.image.load('./sprites/waligie/wfly1.png')
wal_fly1 = pygame.image.load('./sprites/waligie/wfly2.png')

def makeFly(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,32,32)
    entityIdleAnimation = engine.Animation([wal_fly0, wal_fly1])
    entity.animations.add('idle', entityIdleAnimation)
    entity.type = 'player'
    return entity

bullet = pygame.image.load('./sprites/waligie/bullet.png')

def makeBullet(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,16,10)
    entityIdleAnimation = engine.Animation([bullet])
    entity.animations.add('idle', entityIdleAnimation)
    entity.type = 'bullet'
    return entity

wil_idle = pygame.image.load('./sprites/wiligie/wil_stand.png')

wil_walking0 = pygame.image.load('./sprites/wiligie/wil_walk.png')

def makeWil(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,50,52)
    entityIdleAnimation = engine.Animation([wil_idle])
    entityWalkingAnimation = engine.Animation([wil_idle, wil_walking0])
    entity.animations.add('idle', entityIdleAnimation)
    entity.animations.add('walk', entityWalkingAnimation)
    entity.type = 'player'
    return entity

wd_idle = pygame.image.load('./sprites/waluide/wd_stand.png')

wd_walking0 = pygame.image.load('./sprites/waluide/wd_walk.png')

def makeWad(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,34,54)
    entityIdleAnimation = engine.Animation([wd_idle])
    entityWalkingAnimation = engine.Animation([wd_idle, wd_walking0])
    entity.animations.add('idle', entityIdleAnimation)
    entity.animations.add('walk', entityWalkingAnimation)
    entity.type = 'player'
    return entity

# bosses! spoiler alerte wtf???

boss1 = pygame.image.load('./sprites/biden-ingame.jpg')

def makeBiden(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,240,300)
    entityAnimation = engine.Animation([boss1])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'boss'
    return entity

boss2 = pygame.image.load('./sprites/th-marisa.png')

def makeMarisa(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,53,61)
    entityAnimation = engine.Animation([boss2])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'boss'
    return entity

bulb = pygame.image.load('./sprites/bulb.png')
def makeBulb(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,32,51)
    entityAnimation = engine.Animation([bulb])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'bad'
    return entity

spark = pygame.image.load('./sprites/masterspark.png')
def makeLaser(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,343,70)
    entityAnimation = engine.Animation([spark])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'laser'
    return entity

boss3 = pygame.image.load('./sprites/gorbeast-ingame.png')
def makeBeast(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,278,187)
    entityAnimation = engine.Animation([boss3])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'boss'
    return entity

snake = pygame.image.load('./sprites/snake-ingame.png')
def SnakeOne(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,200,200)
    entityAnimation = engine.Animation([snake])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'win'
    return entity

def FairyFodder(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,28,25)
    entityAnimation = engine.Animation([fairy])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'good'
    return entity

def MarisaClone(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,53,61)
    entityAnimation = engine.Animation([boss2])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'bad'
    return entity

realmari = pygame.image.load('./sprites/th-hangedmarisa.png')
def HangedMari(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,53,61)
    entityAnimation = engine.Animation([realmari])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'help'
    return entity

brandon = pygame.image.load('./sprites/dark-brandon-ingame.png')
def DarkBrandon(x,y):
    entity = engine.Entity()
    entity.position = engine.Position(x,y,120,150)
    entityAnimation = engine.Animation([brandon])
    entity.animations.add('idle', entityAnimation)
    entity.type = 'help'
    return entity
