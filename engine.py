import pygame

class System():
    def __init__(self):
        pass
    def check(self, entity):
        return True
    def update(self, screen, entities, platforms):
        for entity in entities:
            if self.check(entity):
                self.updateEntity(screen, entity, entities, platforms)
    def updateEntity(self, screen, entity, entities, platforms):
        pass

class CameraSystem(System):
    def __init__(self):
        super().__init__()
    def check(self, entity):
        return entity.camera is not None
    def updateEntity(self, screen, entity, entities, platforms):

        # set clipping rectangle
        cameraRect = entity.camera.rect
        clipRect = pygame.Rect(cameraRect.x, cameraRect.y, cameraRect.w, cameraRect.h)
        screen.set_clip(clipRect)

        # update camera if tracking an entity
        if entity.camera.entityToTrack is not None:

            trackedEntity = entity.camera.entityToTrack

            currentX = entity.camera.worldX
            currentY = entity.camera.worldY

            targetX = trackedEntity.position.rect.x + trackedEntity.position.rect.w/2
            targetY = trackedEntity.position.rect.y + trackedEntity.position.rect.h/2

            entity.camera.worldX = (currentX * 0.95) + (targetX * 0.05)
            entity.camera.worldY = (currentY * 0.95) + (targetY * 0.05)

        # calculate offsets
        offsetX = cameraRect.x + cameraRect.w/2 - entity.camera.worldX
        offsetY = cameraRect.y + cameraRect.h/2 - entity.camera.worldY

        # fill camera background
        # screen.fill((255,255,255))

        # render platforms
        for p in platforms:
            newPosRect = pygame.Rect(p.x + offsetX, p.y + offsetY, p.w, p.h)
            pygame.draw.rect(screen, (0,0,0), newPosRect)

        # render entities
        for e in entities:
            s = e.state
            a = e.animations.animationList[s]
            a.draw(screen, e.position.rect.x + offsetX, e.position.rect.y + offsetY, e.direction == 'left', False)

        # unset clipping rectangle
        screen.set_clip(None)

class Camera():
    def __init__(self, x, y, w, h):
        self.rect = pygame.Rect(x,y,w,h)
        self.worldX = 0
        self.worldY = 0
        self.entityToTrack = None
    def setWorldPos(self, x, y):
        self.worldX = x
        self.worldY = y
    def trackEntity(self, e):
        self.entityToTrack = e


class Position():
    def __init__(self, x, y, w, h):
        self.rect = pygame.Rect(x,y,w,h)

class Animations():
    def __init__(self):
        self.animationList = {}
    def add(self, state, animation):
        self.animationList[state] = animation

class Animation():
    def __init__(self, images):
        self.images = images
        self.image_index = 0
        self.anim_timer = 0
        self.anim_speed = 8
    def update(self):
        self.anim_timer += 1
        if self.anim_timer >= self.anim_speed:
            self.anim_timer = 0
            self.image_index += 1
            if self.image_index > len(self.images) - 1:
                self.image_index = 0

    def draw(self, screen, x, y, fx, fy):
        # fx and fy are for flipped
        screen.blit(pygame.transform.flip(self.images[self.image_index], fx, fy), (x, y))

class Entity():
    def __init__(self):
        self.state = 'idle'
        self.type = 'normal'
        self.position = None
        self.animations = Animations()
        self.direction = 'left'
        self.camera = None
