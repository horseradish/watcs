# WTF DA SNEK
import pygame
import sys
import engine
import utils

# constant bullshi- i mean variables
screen_size=(800, 600)

# fun game, in it; waligie
pygame.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("waligie and the computer snake")
clock = pygame.time.Clock()
spawnpoint = (600, 600)

pygame.mixer.music.load('./sounds/music/snake.mp3')
pygame.mixer.music.play(-1)

entities = []

# waligie himself
wal_speed = 0
wal_accel = 0.25
wal_jump = -10.5
wal_walk = 5 # 8 is waluide speed actually oof

hurt_sound = pygame.mixer.Sound('./sounds/blahst.wav')
get_sound = pygame.mixer.Sound('./sounds/sugar-collect.wav')
jump_sound = pygame.mixer.Sound('./sounds/quake-huah.wav')
snake_sound = pygame.mixer.Sound('./sounds/uthit.wav')

hit = 0

# platformes
platforms = [
    pygame.Rect(0,700,1000,50),
    pygame.Rect(0,300,200,50),
    pygame.Rect(800,300,200,50),
    pygame.Rect(800,500,200,50),
    pygame.Rect(0,500,200,50),
    pygame.Rect(300,200,400,50),
    pygame.Rect(0,0,200,50),
    pygame.Rect(800,0,200,50),
    pygame.Rect(800,-300,200,50),
    pygame.Rect(0,-300,200,50),
    pygame.Rect(300,-200,400,50)
]

loopleft = [pygame.Rect(-10,-500,50,50000)]
loopright = [pygame.Rect(900,-500,50,50000)]

# entities (waligie, sugar, good burger and the bad feathers)
player = utils.makeWal(spawnpoint[0],spawnpoint[1])
player.camera = engine.Camera(10,10,800,600)
player.camera.setWorldPos(spawnpoint[0],spawnpoint[1])
player.camera.trackEntity(player)
entities.append(player)

entities.append(utils.SnakeOne(400,0))

cameraSys = engine.CameraSystem()

# gam loope
end_it = False
while not end_it:
    # quit conditions (quitter lmfao)
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    new_wal_x = player.position.rect.x
    new_wal_y = player.position.rect.y

    # player stuff
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        new_wal_x -= wal_walk
        player.direction = 'left'
        player.state = 'walk'
    if keys[pygame.K_RIGHT]:
        new_wal_x += wal_walk
        player.direction = 'right'
        player.state = 'walk'
    if keys[pygame.K_UP] and wal_grounded:
        wal_speed = wal_jump
        pygame.mixer.Sound.play(jump_sound)
    if not keys[pygame.K_LEFT] and not keys[pygame.K_RIGHT]:
        player.state = 'idle'

    new_wal_rect=pygame.Rect(new_wal_x, new_wal_y, player.position.rect.width, player.position.rect.height)
    x_col=False

    for p in platforms:
        if p.colliderect(new_wal_rect):
            x_col=True
            break
    if x_col == False:
        player.position.rect.x = new_wal_x

    # update stuff
    for e in entities:
        e.animations.animationList[e.state].update()

    # vertical movement!!
    wal_speed += wal_accel
    new_wal_y += wal_speed

    new_wal_rect=pygame.Rect(int(player.position.rect.x), int(new_wal_y), player.position.rect.width, player.position.rect.height)
    y_col = False
    wal_grounded = False

    for p in platforms:
        if p.colliderect(new_wal_rect):
            y_col=True
            wal_speed=0
            if p[1] > new_wal_y:
                player.position.rect.y = p[1] - player.position.rect.height
                wal_grounded = True
            break

    if y_col == False:
        player.position.rect.y = int(new_wal_y)

    # items like sugar and ham maybe
    wal_rect = pygame.Rect(int(player.position.rect.x), int(player.position.rect.y), int(player.position.rect.width), int(player.position.rect.height))

    for e in entities:
        if e.type == 'good':
            if e.position.rect.colliderect(wal_rect):
                entities.remove(e)
                pygame.mixer.Sound.play(get_sound)
        if e.type == 'bad':
            if e.position.rect.colliderect(wal_rect):
                player.position.rect.x, player.position.rect.y = spawnpoint
                wal_speed = 0
                pygame.mixer.Sound.play(hurt_sound)
        if e.type == 'win':
            if e.position.rect.colliderect(wal_rect):
                hit += 1
                player.position.rect.x, player.position.rect.y = spawnpoint
                pygame.mixer.Sound.play(snake_sound)
                if hit == 2:
                    entities.remove(e)
                    entities.append(utils.SnakeOne(400,-750))
                if hit == 10:
                    end_it = True


    for ll in loopleft:
        if ll.colliderect(wal_rect):
            player.position.rect.x = 800
    for lr in loopright:
        if lr.colliderect(wal_rect):
            player.position.rect.x = 100

    # draw? what am i, an artist?!
    screen.fill( (0,0,50) )

    cameraSys.update(screen, entities, platforms)

    pygame.display.flip()
    clock.tick(60)
