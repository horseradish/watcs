# i have actual university exams for english and italian and yet you shmucks want me to make a game or some shit for YOUR exams. you are all narcissists, damn. anyhow, i do not own waligie (he is owned by everyone) and is just a fangame of some intentionally bad mario fangames basically. disclaimer: game will be intentionally confusing lmao
# also note that idfk how to make levels lol
import pygame
import sys
import engine
import utils

# constant bullshi- i mean variables
screen_size=(800, 600)

# fun game, in it; waligie
pygame.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("waligie and the computer snake")
clock = pygame.time.Clock()
spawnpoint = (100, 50)

pygame.mixer.music.load('./sounds/music/woman.mp3')
pygame.mixer.music.play(-1)

entities = []

# waligie himself (no, wiligie)
wal_speed = 0
wal_accel = 0.65
wal_jump = -19
wal_walk = 3

hurt_sound = pygame.mixer.Sound('./sounds/overused-boom.wav')
get_sound = pygame.mixer.Sound('./sounds/sugar-collect.wav')
jump_sound = pygame.mixer.Sound('./sounds/caveman-sound.wav')

# platformes
platforms = [
    pygame.Rect(10,240,250,50),
    pygame.Rect(100,600,400,50),
    pygame.Rect(450,250,400,50),
    pygame.Rect(300,950,800,50)
]

killer_rect = [pygame.Rect(-500,2000,50000,50)]

# entities (waligie, sugar, good burger and the bad feathers)
entities.append(utils.makeSugar(400,260))
entities.append(utils.makeSugar(200,450))

entities.append(utils.makeFeather(500,600))
entities.append(utils.makeFeather(400,100))

player = utils.makeWil(100,50)
player.camera = engine.Camera(10,10,800,600)
player.camera.setWorldPos(100,50)
player.camera.trackEntity(player)
entities.append(player)

entities.append(utils.makeFinish(1000,480))

cameraSys = engine.CameraSystem()

# gam loope
end_it = False
while not end_it:
    # quit conditions (quitter lmfao)
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    new_wal_x = player.position.rect.x
    new_wal_y = player.position.rect.y

    # player stuff
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        new_wal_x -= wal_walk
        player.direction = 'left'
        player.state = 'walk'
    if keys[pygame.K_RIGHT]:
        new_wal_x += wal_walk
        player.direction = 'right'
        player.state = 'walk'
    if keys[pygame.K_UP] and wal_grounded:
        wal_speed = wal_jump
        pygame.mixer.Sound.play(jump_sound)
    if not keys[pygame.K_LEFT] and not keys[pygame.K_RIGHT]:
        player.state = 'idle'

    new_wal_rect=pygame.Rect(new_wal_x, new_wal_y, player.position.rect.width, player.position.rect.height)
    x_col=False

    for p in platforms:
        if p.colliderect(new_wal_rect):
            x_col=True
            break

    if x_col == False:
        player.position.rect.x = new_wal_x

    # update stuff
    for e in entities:
        e.animations.animationList[e.state].update()

    # vertical movement!!
    wal_speed += wal_accel
    new_wal_y += wal_speed

    new_wal_rect=pygame.Rect(int(player.position.rect.x), int(new_wal_y), player.position.rect.width, player.position.rect.height)
    y_col = False
    wal_grounded = False

    for p in platforms:
        if p.colliderect(new_wal_rect):
            y_col=True
            wal_speed=0
            if p[1] > new_wal_y:
                player.position.rect.y = p[1] - player.position.rect.height
                wal_grounded = True
            break

    if y_col == False:
        player.position.rect.y = int(new_wal_y)

    # items like sugar and ham maybe
    wal_rect = pygame.Rect(int(player.position.rect.x), int(player.position.rect.y), int(player.position.rect.width), int(player.position.rect.height))

    for e in entities:
        if e.type == 'good':
            if e.position.rect.colliderect(wal_rect):
                entities.remove(e)
                pygame.mixer.Sound.play(get_sound)
        if e.type == 'bad':
            if e.position.rect.colliderect(wal_rect):
                player.position.rect.x, player.position.rect.y = spawnpoint
                wal_speed = 0
                pygame.mixer.Sound.play(hurt_sound)
        if e.type == 'win':
            if e.position.rect.colliderect(wal_rect):
                end_it = True

    for k in killer_rect:
        if k.colliderect(wal_rect):
                player.position.rect.x, player.position.rect.y = spawnpoint
                wal_speed = 0
                pygame.mixer.Sound.play(hurt_sound)

    # draw? what am i, an artist?!
    screen.fill( (255,255,255) )

    cameraSys.update(screen, entities, platforms)

    pygame.display.flip()
    clock.tick(60)
